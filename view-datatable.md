# View Datatable

* Create Method GET in Service Layer
* Call Data Service in Controller

* Extract data in HTML

# Create Method Save in Service Layer

```java
public List<Employee> getAllEmployee(){
    List<Employee> employees = this.employeeRepository.findAll();
    return employees;
}
```

```
Passing Data from Controller to View
```

```
## Extract data in HTML
```

## Call Data Service in Controller

```java
@RequestMapping("/form")
public String myform(Model model, @RequestParam(name="submit", required = false) String submitStatus) {
    model.addAttribute("employee", new Employee());
    //get all data employee

    List<Employee> employees = this.employeeService.getAllEmployee();
    model.addAttribute("employees", employees);

    if(submitStatus != null && submitStatus.equals("1")) {
        model.addAttribute("submitStatus", true);
    }
    return "myform";
}
```

## Extract Data in HTML

```java
<div>
	<h2>View Data Employee</h2>
	<table border=1>
		<tr>
			<th>Name</th>
			<th>Address</th>
			<th>Job Description</th>		
		</tr>
	   <th:block th:each="emp : ${employees}">
	      <tr>
	         <td th:text="${emp.name}"></td>
	         <td th:text="${emp.address}"></td>
	         <td th:text="${emp.jobDesc}"></td>
	      </tr>
	   </th:block>
	</table>
</div>
```



