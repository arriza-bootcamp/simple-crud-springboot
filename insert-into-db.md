# Insert into DB

* entity annotation in Employee class

* create Service

* create Repository

# Entity annotation in Employee class

```java
@Entity
@Table(name="employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String address;
    private String jobDesc;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getJobDesc() {
        return jobDesc;
    }
    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc;
    }

}
```

## Create Repository / DAO

```java
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
```

## Create Service

```java
@Service
@Transactional
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public void save(Employee employee) {
         employeeRepository.save(employee);
    }
}
```

## Call Service Layer

```java
@Autowired
private EmployeeService employeeService;

@PostMapping("/proses")
public String prosesForm(@ModelAttribute("employee") Employee employee) {
    employeeService.save(employee);
    return "redirect:/form?submit=1";
}
```



