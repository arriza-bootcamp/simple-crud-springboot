# Hello World

* Create Controller
* Running program

## Create Controller

```java
@Controller
@RequestMapping("/")
public class Home {

    @RequestMapping
    @ResponseBody
    public String index() {
        return "hello world for java spring";
    }
}    
```

## Running Program

* http://localhost:8080/



