# Summary

* [Introduction](README.md)
* [1. Installation](installation.md)
* [2. running program & HelloWorld](helloworld.md)
* [3. Data Form](data-form.md)
* [4. Proses Data Input](proses-data-input.md)
* [5. Insert into DB](insert-into-db.md)
* [6. View Datatable](view-datatable.md)

