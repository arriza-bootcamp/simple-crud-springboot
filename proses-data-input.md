# Proses Data Input

* set url in th:action
* sdasdasd

## Controller && Show Message

```java
@PostMapping("/proses")
public String prosesForm(@ModelAttribute("employee") Employee employee) {
    System.out.println("employee name : "+ employee.getName());
    return "redirect:/form?submit=1";
}
```

```java
@RequestMapping("/form")
public String myform(Model model, @RequestParam(name="submit", required = false) String submitStatus) {
    model.addAttribute("employee", new Employee());
    if(submitStatus != null && submitStatus.equals("1")) {
        model.addAttribute("submitStatus", true);
    }
    return "myform";
}
```

```java
<!DOCTYPE HTML>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8"/>
    <title>Spring Boot Thymeleaf Application - Form Samples - Inputs</title>
</head>
<body>

<h2 th:if = '${submitStatus == true}'>You can do better!</h2>

<form th:action="@{/proses}" th:object="${employee}" method="post">

    <div>
        <label>name:</label>
        <input type="text" th:field="*{name}" placeholder="enter your name" />
    </div>
    <div>
        <label>address:</label>
        <input type="text" th:field="*{address}" placeholder="enter your address" />
    </div>
     <div>
        <label>description:</label>
        <input type="text" th:field="*{jobDesc}" placeholder="enter your job description" />
    </div>
    <input type="submit" value="Submit"/>
</form>

</body>
</html>
```



