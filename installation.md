# Springboot Installation

* Site
* Dependency
* Database Connection

## Site

* [https://start.spring.io/](https://start.spring.io/)

## Dependency

* **Spring Web**

* **Spring Data JPA**

* **Spring Boot DevTools**

* **Thymeleaf**

* **postgre / mysql**

## Database Connection

### Mysql

```
spring.jpa.hibernate.ddl-auto=create
spring.datasource.url=jdbc:mysql://localhost:3306/sample_basic_spring_security
spring.datasource.username=root
spring.datasource.password=
```

### Postgresql

```
spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.PostgreSQLDialect
spring.jpa.hibernate.ddl-auto=create
spring.jpa.hibernate.show-sql=true
spring.datasource.url=jdbc:postgresql://localhost:5432/db_mybank
spring.datasource.username=postgres
spring.datasource.password=
```



