# Data Form di Spring

* create entity
* passing controller
* html / thymleaf

## Entity

```java
@Entity
@Table(name="employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String address;
    private String jobDesc;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getJobDesc() {
        return jobDesc;
    }
    public void setJobDesc(String jobDesc) {
        this.jobDesc = jobDesc;
    }

}
```

## Passing Controller

```java
@RequestMapping("/form")
public String myform(Model model) {
    model.addAttribute("employee", new Employee());
    return "myform"; //search myform.html
}
```

## HTML form with Thymleaf

* th:object
* th:field
* th:action
* th:text

```java
<!DOCTYPE HTML>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8"/>
    <title>Spring Boot Thymeleaf Application - Form Samples - Inputs</title>
</head>
<body>

<form th:action="@{/proses}" th:object="${employee}" method="post">

    <div>
        <label>name:</label>
        <input type="text" th:field="*{name}" placeholder="enter your name" />
    </div>
    <div>
        <label>address:</label>
        <input type="text" th:field="*{address}" placeholder="enter your address" />
    </div>
     <div>
        <label>description:</label>
        <input type="text" th:field="*{jobDesc}" placeholder="enter your job description" />
    </div>
    <input type="submit" value="Submit"/>
</form>

</body> 
</html>
```



